package com.park.oss.web.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.park.oss.model.CarPlace;
import com.park.oss.service.CarPlaceService;
import com.park.oss.service.PlaceService;
import com.park.oss.util.ParkUtil;

@Controller
@RequestMapping("/client")
public class ClientController {
	@Autowired
	CarPlaceService service;

	@Autowired
	PlaceService placeSevice;
	
	@Autowired
	CarPlaceService  carPlaceService;

	@RequestMapping(value = "/entrance", method = RequestMethod.GET)
	public @ResponseBody String entrance(@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "date", required = true) String date) {

		Long carPlaceId = ParkUtil.decode(id);
		// CarPlace place = service.getCarPlace(carPlaceId);
		placeSevice.enter(carPlaceId.intValue());
		return "success";
	}

	@RequestMapping(value = "/exit", method = RequestMethod.GET)
	public @ResponseBody String exit(@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "date", required = true) String date) {
		Long carPlaceId = ParkUtil.decode(id);
		placeSevice.exit(carPlaceId.intValue());
		return "success";
	}
	
	@RequestMapping(value = "/status", method = RequestMethod.GET)
	public @ResponseBody String exit(@RequestParam(value = "id", required = true) long id) {
		CarPlace cp = carPlaceService.getCarPlace(id);
		return ""+cp.getStatus();
	}
	
	
	
}
