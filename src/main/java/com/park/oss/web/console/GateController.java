package com.park.oss.web.console;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.park.oss.model.Gate;
import com.park.oss.service.GateService;
import com.park.oss.service.ParkService;

@Controller
@RequestMapping("/gate")
public class GateController {
	@Autowired
	GateService service ;
	@Autowired
	ParkService parkService ;
    @RequestMapping(value = "/index.html", method = RequestMethod.GET)
    public ModelAndView index(Integer parkId) {
    	ModelAndView view = new ModelAndView("/gate/index.btl");
    	List<Gate> list = service.getGates(parkId);
    	view.addObject("list",list);
    	view.addObject("park",parkService.getPark(parkId));
     return view;
    }
    
   
    @RequestMapping(value = "/add.html", method = RequestMethod.GET)
    public ModelAndView addPage(Integer parkId) {
	    	ModelAndView view = new ModelAndView("/gate/edit.btl");
	    	view.addObject("parkId", parkId);
	    return view;
    }
    
    @RequestMapping(value = "/edit.html", method = RequestMethod.GET)
    public ModelAndView editPage(Integer id) {
	    	ModelAndView view = new ModelAndView("/gate/edit.btl");
	    	Gate gate = service.getGate(id);
	    	view.addObject("parkId",gate.getParkId());
	    	view.addObject("gate", gate);
	    return view;
    }
    
    @RequestMapping(value = "/editAction.html", method = RequestMethod.POST)
    public ModelAndView editPage(Gate gate,Integer actionType) {
    		if(actionType==0){
    			service.addGate(gate);
    		}else{
    			service.updateGate(gate);
    		}
    		
    		return index(gate.getParkId());
    }
    
    @RequestMapping(value = "/deleteAction.html", method = RequestMethod.GET)
    public ModelAndView remove(Integer id) {
    		Gate gate = service.getGate(id);
    		service.removeGate(id);
    		
    		return index(gate.getParkId());
    }
//    
//    @RequestMapping(value = "/editAction.html", method = RequestMethod.POST)
//    public ModelAndView editAction(Park park,Integer actionType) {
//    		if(actionType==0){
//    			park.setFreeAmount(0);
//    			park.setUpdateTime(new Timestamp(new Date().getTime()));
//    			service.addPark(park);
//    		}else{
//    			service.updatePark(park);
//    		}
//	    
//	    	return index();
//    }
//    
//    @RequestMapping(value = "/income.html", method = RequestMethod.GET)
//    public ModelAndView sum(Integer parkId) {
//    		List<ParkSum> list=  service.sum(parkId);
//    	 	ModelAndView view = new ModelAndView("/park/income.btl");
//	    	view.addObject("list", list);
//	    	return view;
//    }
}
