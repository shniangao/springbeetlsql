package com.park.oss.web.console;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.park.oss.model.Payment;
import com.park.oss.model.PlaceRent;
import com.park.oss.model.PlaceRentRule;
import com.park.oss.model.SysUser;
import com.park.oss.service.UserService;

@Controller
@RequestMapping("/user")
public class SysUserController {
	@Autowired
	UserService service ;
    
	@RequestMapping(value = "/index.html", method = RequestMethod.GET)
    public ModelAndView index(@RequestParam(value="parkId", required=false) Integer parkId,
    		@RequestParam(value="hasPlace", required=false) Integer hasPlace
    		) {
	    	ModelAndView view = new ModelAndView("/user/index.btl");
	    	List<SysUser> list = service.query(parkId,hasPlace);
	    	view.addObject("list",list);
        return view;
    }
	
	
	@RequestMapping(value = "/payment.html", method = RequestMethod.GET)
    public ModelAndView payment(@RequestParam(value="userId", required=true) Integer userId) {
    	ModelAndView view = new ModelAndView("/user/payment.btl");
    	List<Payment> list = service.queryPayment(userId);
    	view.addObject("list",list);
     	view.addObject("ownerId", userId);
        return view;
    }
	
	
	@RequestMapping(value = "/sheduleRule.html", method = RequestMethod.GET)
    public ModelAndView sheduleRule(@RequestParam(value="userId", required=true) Integer userId) {
    	ModelAndView view = new ModelAndView("/user/shedule.btl");
    	PlaceRentRule rent = service.getRentRule(userId);
    	view.addObject("rent",rent);
    	view.addObject("ownerId", userId);
    return view;
    }
	
	@RequestMapping(value = "/shedule.html", method = RequestMethod.GET)
    public ModelAndView shedule(@RequestParam(value="userId", required=true) Integer userId) {
	    	ModelAndView view = new ModelAndView("/user/rent.btl");
	    	List<PlaceRent> rent = service.getRent(userId);
	    	view.addObject("list",rent);
	    	view.addObject("user",service.getUser(userId));
	    	   
	    return view;
    }
    
    
  
}
