package com.park.oss.model;
import java.sql.Timestamp;

import org.beetl.sql.core.TailBean;

/*
* 
* gen by beetlsql 2016-01-09
*/
public class Payment extends TailBean {
	private Integer id ;
	private Integer balanceAmount ;
	private Integer carPlaceId ;
	private Integer couponAmount ;
	private Integer ownerId ;
	private Integer parkId ;
	private Integer placeId ;
	private Integer userId ;
	private Double amount ;
	private String mobile ;
	private Double ownerMoney ;
	private Double parkMoney ;
	private String plate ;
	private Double platformMoney ;
	private Timestamp createTime ;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(Integer balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	public Integer getCarPlaceId() {
		return carPlaceId;
	}
	public void setCarPlaceId(Integer carPlaceId) {
		this.carPlaceId = carPlaceId;
	}
	public Integer getCouponAmount() {
		return couponAmount;
	}
	public void setCouponAmount(Integer couponAmount) {
		this.couponAmount = couponAmount;
	}
	public Integer getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}
	public Integer getParkId() {
		return parkId;
	}
	public void setParkId(Integer parkId) {
		this.parkId = parkId;
	}
	public Integer getPlaceId() {
		return placeId;
	}
	public void setPlaceId(Integer placeId) {
		this.placeId = placeId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public Double getOwnerMoney() {
		return ownerMoney;
	}
	public void setOwnerMoney(Double ownerMoney) {
		this.ownerMoney = ownerMoney;
	}
	public Double getParkMoney() {
		return parkMoney;
	}
	public void setParkMoney(Double parkMoney) {
		this.parkMoney = parkMoney;
	}
	public String getPlate() {
		return plate;
	}
	public void setPlate(String plate) {
		this.plate = plate;
	}
	public Double getPlatformMoney() {
		return platformMoney;
	}
	public void setPlatformMoney(Double platformMoney) {
		this.platformMoney = platformMoney;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

}
