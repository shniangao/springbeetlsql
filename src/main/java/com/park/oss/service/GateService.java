package com.park.oss.service;
import java.util.List;

import com.park.oss.model.Gate;

public interface GateService {
	public void addGate(Gate gate);
	public Gate getGate(Integer id);
	public List<Gate> getGates(Integer parkId);
	public void updateGate(Gate gate);
	public void removeGate(Integer id);
}