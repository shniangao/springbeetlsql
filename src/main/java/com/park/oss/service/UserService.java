package com.park.oss.service;

import java.util.List;

import com.park.oss.model.Payment;
import com.park.oss.model.PlaceRent;
import com.park.oss.model.PlaceRentRule;
import com.park.oss.model.SysUser;
import com.park.oss.model.UserBalance;

public interface UserService {
	SysUser getUser(Integer userId);
	SysUser getUser(String mobile,String password);
	SysUser addUser(String mobile,String pwd);
	void updateUser(SysUser user);
	List<SysUser> query(Integer parkId,Integer hasPlace);
	List<Payment> queryPayment(Integer userId);
	PlaceRentRule getRentRule(Integer userId);
	List<PlaceRent> getRent(Integer userId);
	UserBalance getBalance(Integer userId);
	
}
