package com.park.oss.service;

import java.util.List;

import org.beetl.sql.core.engine.PageQuery;

import com.park.oss.model.Gate;
import com.park.oss.model.Park;
import com.park.oss.vo.ParkSum;

public interface ParkService {
	void queryAll(PageQuery pageQuery);
	Park getPark(Integer id);
	public void addPark(Park park);
	public void updatePark(Park park);
	public void addGate(Gate gate);
	public void removeGate(Integer id);
	public List<ParkSum> sum(Integer parkId);
	
}
